ARG PHP_VERSION="7.4"
FROM php:${PHP_VERSION}-fpm-alpine

ARG PSR_VERSION="1.0.1"
ARG PHALCON_VERSION="4.1.0"

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN ln -sf /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini \
    # PHP zip
    && install-php-extensions zip \
    # Composer
    && curl -sL https://getcomposer.org/installer | php -- --install-dir /usr/bin --filename composer

RUN curl -LO https://github.com/jbboehr/php-psr/archive/v${PSR_VERSION}.tar.gz \
    && tar xzf v${PSR_VERSION}.tar.gz \
    && docker-php-ext-install -j$(nproc) ${PWD}/php-psr-${PSR_VERSION} \
    && rm -rf v${PSR_VERSION}.tar.gz php-psr-${PSR_VERSION}

RUN curl -LO https://github.com/phalcon/cphalcon/archive/v${PHALCON_VERSION}.tar.gz \
    && tar xzf v${PHALCON_VERSION}.tar.gz \
    && docker-php-ext-install -j$(nproc) ${PWD}/cphalcon-${PHALCON_VERSION}/build/php7/64bits \
    && rm -rf v${PHALCON_VERSION}.tar.gz cphalcon-${PHALCON_VERSION}

RUN apk --no-cache add supervisor nginx

COPY config/fpm-pool.conf 		/usr/local/etc/php-fpm.d/app.conf
COPY config/php.ini 			/usr/local/etc/php/conf.d/app.ini
COPY config/nginx.conf 			/etc/nginx/nginx.conf
COPY config/supervisord.conf 	/etc/supervisor/conf.d/supervisord.conf

CMD /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf

WORKDIR /var/www/html

COPY src ./

ARG ENVIRONMENT="development"

RUN if [ "${ENVIRONMENT}" == "production" ]; then \
        php /usr/bin/composer install --no-interaction --no-ansi --no-progress --no-dev --no-suggest --classmap-authoritative --optimize-autoloader \
    ; else \
        install-php-extensions xdebug && \
        echo "xdebug.mode=develop,coverage" >> /usr/local/etc/php/php.ini-development && \
        ln -sf /usr/local/etc/php/php.ini-development /usr/local/etc/php/php.ini && \
        php /usr/bin/composer install --no-interaction --no-ansi --no-progress --optimize-autoloader \
    ; fi \
    && composer clear-cache \
    && rm -rf /var/cache/apk \
    && mkdir /var/cache/apk \
    && rm -rf /tmp/*

EXPOSE 80

HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:80/fpm-ping