# curve-tech-challenge

##Objective
Structure an api in a way that is testable, deployable and extensible

## Running
### Build
`docker build --build-arg ENVIRONMENT=production -t curve-tech-challenge .`

**Important note:** *This docker image is based on official php:7.4-fpm-alpine. It's is required to have the docker client logged on DockerHub to download it.*

### Run
`docker run -dp 80:80 --name curve-app curve-tech-challenge`

*The app should now be accessible on localhost at the specified port. /gbp and /usd should return expected responses.*

### Stop
`docker stop curve-app`

##Development
This project makes use of docker-compose and Makefile to simplify bringing up a development env, aswell as running operations (dependencies update, testing, etc).

###Run (downloads image deps & autobuilds if necessary)
`make up`
###Stop
`make down`

###Display additional shortcuts
`make help`