<?php

namespace Tests\Unit;

use App\Controllers\BaseController as Controller;
use Codeception\Test\Unit;

class BaseControllerTest extends Unit
{
    public Controller $controller;

    protected function setUp(): void
    {
        parent::setUp();
        $this->controller = new Controller();
    }

    public function testInit()
    {

        $this->assertEquals(
            201,
            $this->controller->response->getStatusCode()
        );

        $this->assertEquals(
            'application/json; charset=' . $this->controller->config->encoding,
            $this->controller->response->getHeaders()->get('Content-Type')
        );
    }
}