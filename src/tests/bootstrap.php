<?php

declare(strict_types=1);

xdebug_start_code_coverage();

use Mvc\Application;
use Phalcon\Di\FactoryDefault;

require __DIR__ . '/../app/Application.php';

return new Application(new FactoryDefault());
