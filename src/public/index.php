<?php

declare(strict_types=1);

use Mvc\Application;
use Phalcon\Di\FactoryDefault;

if (getenv('ENVIRONMENT') !== 'production') {
    require __DIR__ . '/../c3.php';
}

try {
    require __DIR__ . '/../app/Application.php';
    $app = new Application(new FactoryDefault());
    $app->handle($app->request->getURI());
    $app->response->send();
} catch (Exception $e) {
    if (APPLICATION_ENV === Application::APPLICATION_ENV_DEVELOPMENT) {
        print_r($e->getMessage() . '<br>');
        print_r('<pre>' . $e->getTraceAsString() . '</pre>');
    }
}
