<?php

declare(strict_types=1);

use Phalcon\Config;

return new Config(
    [
        'encoding' => 'UTF-8',
        'locale' => 'en_EN.UTF-8',
        'timezone' => 'Europe/Lisbon',
        'api' => [
            'DAO' => 'ExchangeRatesAPI',
            'endpoint' => 'https://api.exchangeratesapi.io/',
        ],
    ]
);
