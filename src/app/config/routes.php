<?php

declare(strict_types=1);

use Phalcon\Config;
use App\Controllers\IndexController;
use App\Controllers\ExampleController;

return new Config(
    [
        'routes' => [
            'index' => [
                'handler' => IndexController::class,
                'lazy' => true,
                'prefix' => null,
                'endpoints' => [
                    [
                        'method' => 'get',
                        'routePattern' => '/{base}',
                        'handler' => 'indexAction',
                    ],
                ],
            ],
            'example' => [
                'handler' => ExampleController::class,
                'lazy' => true,
                'prefix' => '/example',
                'endpoints' => [
                    [
                        'method' => 'get',
                        'routePattern' => '/get',
                        'handler' => 'getAction',
                    ],
                    [
                        'method' => 'post',
                        'routePattern' => '/post',
                        'handler' => 'postAction',
                    ],
                    [
                        'method' => 'patch',
                        'routePattern' => '/patch',
                        'handler' => 'patchAction',
                    ],
                ],
            ],
        ],
    ]
);
