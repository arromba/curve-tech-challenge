<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Models\DAOFactory;
use App\Models\Request;
use Exception;

class IndexController extends BaseController
{

    public function indexAction(string $base): void
    {
        try {
            $api = (new DAOFactory())
                ->build(new Request())
                ->setBase($base);

            $latest = $api->getLatest();
            $lastWeek = $api->getLastWeekRate($latest->date);
        } catch (Exception $e) {
            (new ErrorController())->errorAction();

            return;
        }

        $this->response->setJsonContent(
            [
                'status' => 'success',
                'base' => strtoupper($base),
                'EUR' => $latest->rates->EUR,
                'exchange' => $latest->rates->EUR > $lastWeek->rates->EUR,
//                'rawApiData' => [
//                    'info' => 'Debug purposes',
//                    'latest' => $latest,
//                    'lastWeek' => $lastWeek,
//                ]
            ]
        );
    }
}
