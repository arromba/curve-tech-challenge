<?php

declare(strict_types=1);

namespace App\Controllers;

use Exception;

class ErrorController extends BaseController
{

    public function notFoundAction(): void
    {
        $this->response->setStatusCode(404, 'Not Found');
        $this->response->setJsonContent(
            [
                'status' => 'error',
                'code' => 404,
                'messages' => 'Not found',
            ]
        );
    }

    public function errorAction(): void
    {
        $this->response->setStatusCode(500, 'Internal Server Error');
        $this->response->setJsonContent(
            [
                'status' => 'error',
                'code' => 500,
                'messages' => 'Internal Server Error',
            ]
        );
    }
}
