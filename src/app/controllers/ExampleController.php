<?php

declare(strict_types=1);

namespace App\Controllers;

class ExampleController extends BaseController
{

    public function getAction(): void
    {
        $this->response->setJsonContent(
            [
                'status' => 'success',
                'messages' => 'GET request example',
            ]
        );
    }

    public function postAction(): void
    {
        $this->response->setJsonContent(
            [
                'status' => 'success',
                'messages' => 'POST request example',
            ]
        );
    }

    public function patchAction(): void
    {
        $this->response->setJsonContent(
            [
                'status' => 'success',
                'messages' => 'PATCH request example',
            ]
        );
        $this->response->send();
    }
}
