<?php

declare(strict_types=1);

namespace App\Controllers;

use Phalcon\DI\Injectable as Controller;

/**
 * @property mixed|null config
 */
class BaseController extends Controller
{
    public function __construct()
    {
        $this->response->setStatusCode(201, 'Created');
        $this->response->setContentType('application/json', $this->config->encoding);
    }
}
