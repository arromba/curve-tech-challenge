<?php

declare(strict_types=1);

namespace Mvc;

use Phalcon\Debug;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\Collection;
use App\Providers\ConfigProvider;
use App\Providers\SessionProvider;
use App\Controllers\ErrorController;

/**
 * @property mixed|null config
 */
class Application extends Micro
{
    public const APPLICATION_ENV_PRODUCTION = 'production';
    public const APPLICATION_ENV_DEVELOPMENT = 'development';


    private array $providers = [
        ConfigProvider::class,
        SessionProvider::class,
    ];

    public function __construct(DiInterface $container = null)
    {
        parent::__construct($container);

        define(
            'APPLICATION_ENV',
            getenv('ENVIRONMENT') ?: Application::APPLICATION_ENV_PRODUCTION
        );

        if (APPLICATION_ENV === self::APPLICATION_ENV_DEVELOPMENT) {
            ini_set('display_errors', 'On');
            error_reporting(E_ALL);
            (new Debug())->listen();
        }

        require __DIR__ . '/../vendor/autoload.php';

        $this->registerProviders();
        $this->registerRoutes();

        mb_internal_encoding($this->config->encoding);
        setlocale(LC_TIME, $this->config->locale);
        date_default_timezone_set($this->config->timezone);

        $this->setDI($container);
    }

    private function registerProviders(): void
    {
        foreach ($this->providers as $provider) {
            $this->di->register(new $provider());
        }
    }

    private function registerRoutes(): void
    {
        foreach ($this->config->routes as $route) {
            $collection = new Collection();
            $collection->setHandler($route->handler, $route->lazy);

            if ($route->prefix) {
                $collection->setPrefix($route->prefix);
            }

            foreach ($route->endpoints as $endpoint) {
                $collection->{$endpoint->method}(
                    $endpoint->routePattern,
                    $endpoint->handler
                );
            }

            $this->mount($collection);
        }

        $this->notFound([new ErrorController(), 'notFoundAction']);
        $this->error([new ErrorController(), 'errorAction']);
    }
}
