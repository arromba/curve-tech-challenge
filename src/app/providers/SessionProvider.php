<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Session\Adapter\Stream as SessionAdapter;
use Phalcon\Session\Manager as SessionManager;

class SessionProvider implements ServiceProviderInterface
{
    public function register(DiInterface $di): void
    {
        $di->setShared('session', function (): SessionManager {
            $session = new SessionManager();
            $session->setAdapter(new SessionAdapter([
                'savePath' => sys_get_temp_dir(),
            ]));
            $session->start();

            return $session;
        });
    }
}
