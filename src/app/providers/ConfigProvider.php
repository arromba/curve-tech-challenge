<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface
{
    public function register(DiInterface $di): void
    {
        $di->set('config', function (): Config {
            $config = require __DIR__ . '/../config/config.php';
            $config->merge(require(__DIR__ . '/../config/routes.php'));

            return $config;
        });
    }
}
