<?php

declare(strict_types=1);

namespace App\Models;

use Phalcon\Mvc\Model as Model;
use Exception;

class DAOFactory extends Model
{
    public function build(Request $request): DAO
    {
        $class = __NAMESPACE__ .
            '\\' .
            $this->container->getShared('config')->get('api')['dao'];

        if (!class_exists($class)) {
            throw new Exception('API Object not found');
        }

        return new $class($request);
    }
}
