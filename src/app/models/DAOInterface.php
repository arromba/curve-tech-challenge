<?php

declare(strict_types=1);

namespace App\Models;

interface DAOInterface {
    public function onConstruct(Request $request): void;
    public function setEndpoint(string $endpoint): DAO;
    public function setParams(array $params): DAO;
    public function setParam(string $key, string $value): DAO;
    public function getData(): object;

    public function setBase(string $base): DAO;
    public function getLatest(): object;
    public function getLastWeekRate(string $date): object;
}
