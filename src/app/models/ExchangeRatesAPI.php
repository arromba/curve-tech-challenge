<?php

declare(strict_types=1);

namespace App\Models;

use Exception;

class ExchangeRatesAPI extends DAO implements DAOInterface
{
    public function onConstruct(Request $request): void
    {
        parent::onConstruct($request);

        $this->setParam('symbols', 'EUR');
    }

    public function setBase(string $base): DAO
    {
        $base = strtoupper($base);

        if (!in_array($base, ['GBP', 'USD'])) {
            throw new Exception('Base Currency not supported');
        }

        $this->setParam('base', $base);

        return $this;
    }

    public function getLatest(): object
    {
        return $this->setEndpoint('latest')->getData();
    }

    public function getLastWeekRate(string $date): object
    {
        $lastWeek = date('Y-m-d', strtotime('-1 week', strtotime($date)));

        return $this->setEndpoint($lastWeek)->getData();
    }
}
