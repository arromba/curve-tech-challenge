<?php

declare(strict_types=1);

namespace App\Models;

use Phalcon\Mvc\Model as Model;

/**
 * @property mixed|null config
 */
abstract class DAO extends Model implements DAOInterface
{
    private Request $request;
    private string  $url;
    private string  $endpoint;
    private array   $params;

    public function onConstruct(Request $request): void
    {
        $this->request = $request;
        $this->url = $this->endpoint =
            $this->container->getShared('config')->get('api')['endpoint'];
        $this->params = [];
    }

    public function setEndpoint(string $endpoint): DAO
    {
        $this->endpoint = $this->url . $endpoint;

        return $this;
    }

    public function setParams(array $params): DAO
    {
        $this->params = $params;

        return $this;
    }

    public function setParam(string $key, string $value): DAO
    {
        $this->params[$key] = $value;

        return $this;
    }

    public function getData(): object
    {
        return $this->request->get(
            $this->endpoint . '?' . http_build_query($this->params)
        );
    }
}