<?php

declare(strict_types=1);

namespace App\Models;

use GuzzleHttp\Client;

/*
 * This is a wrong simplistic implementation done on purpose for discussion
 * TODO:
 * It should be a factory permitting interchangeable HTTP Client Libs
 * It should accept dependency injection to allow mock responses for testing
 * It should throw proper exceptions, handle response status codes
 * It should handle json_decode properly, checking for errors
 * Etc
 */

class Request
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function get(string $url): object
    {
        return json_decode(
            $this->client->get($url)->getBody()->getContents()
        );
    }
}
