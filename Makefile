.DEFAULT_GOAL := help
.PHONY: up down install test.unit test.functional test.acceptance test test.coverage test.coverage \
php.metrics.open php.codesniffer php.codesniffer.fix php.messdetector build health logs \
composer.update composer.install help

DOCKER_C := docker-compose
APP_NAME := app
DOCK_X_APP := $(DOCKER_C) exec $(APP_NAME)

PHP_METRICS := /usr/local/lib/php-code-quality/vendor/bin/phpmetrics

VENDOR_PHINX := ./vendor/bin/phinx
VENDOR_CODECEPT := ./vendor/bin/codecept
VENDOR_PHPCS := ./vendor/squizlabs/php_codesniffer/bin/phpcs
VENDOR_PHPCBF := ./vendor/squizlabs/php_codesniffer/bin/phpcbf
VENDOR_METRICS := ./vendor/phpmetrics/phpmetrics/bin/phpmetrics
VENDOR_PHPMD := ./vendor/phpmd/phpmd/src/bin/phpmd

OUTPUT_COVERAGE := src/tests/_output/coverage/
OUTPUT_METRICS := src/tests/_output/php_code_quality/metrics_results/

up: ## Start docker container
	$(DOCKER_C) pull
	$(DOCKER_C) up -d

down: ## Stop docker container
	$(DOCKER_C) down

install: up composer.update ## Install the container & all the dependencies
	$(DOCKER_C) exec -T $(APP_NAME)

test.unit: src/vendor ## Run unit tests suite
	$(DOCK_X_APP) php $(VENDOR_CODECEPT) run unit

test.functional: src/vendor ## Run functional tests suite
	$(DOCK_X_APP) php $(VENDOR_CODECEPT) run functional

test.acceptance: src/vendor ## Run acceptance tests suite
	$(DOCK_X_APP) php $(VENDOR_CODECEPT) run acceptance

test: src/vendor ## Run all available tests
	$(DOCK_X_APP) php $(VENDOR_CODECEPT) run

test.debug: src/vendor ## Run all available tests
	$(DOCK_X_APP) php $(VENDOR_CODECEPT) run -vvv

test.coverage: ## Check project test coverage
	$(DOCK_X_APP) php $(VENDOR_CODECEPT) run --coverage --coverage-html
	open $(OUTPUT_COVERAGE)index.html >&- 2>&- || \
	xdg-open $(OUTPUT_COVERAGE)index.html >&- 2>&- || \
	gnome-open $(OUTPUT_COVERAGE)index.html >&- 2>&-

php.metrics: ## Run php metrics & open metrics web
	$(DOCKER_C) exec $(APP_NAME) php \
	$(VENDOR_METRICS) --excluded-dirs 'vendor','tests' \
	--report-html=./tests/_output/php_code_quality/metrics_results .
	make php.metrics.open

php.metrics.open:
	open $(OUTPUT_METRICS)index.html >&- 2>&- || \
	xdg-open $(OUTPUT_METRICS)index.html >&- 2>&- || \
	gnome-open $(OUTPUT_METRICS)index.html >&- 2>&-

php.cs: ## Run php code sniffer
	$(DOCK_X_APP) php $(VENDOR_PHPCS) \
	-sv --extensions=php,phtml --standard=PSR12 --ignore=vendor,tests,c3.php .

php.cs.fix: ## Run php Code Beautifier and Fixer
	$(DOCK_X_APP) php $(VENDOR_PHPCBF) \
	-sv --extensions=php,phtml --standard=PSR12 --ignore=vendor,tests,c3.php .

php.messdetector: ## Run php mess detector
	$(DOCK_X_APP) php $(VENDOR_PHPMD) . \
	text cleancode,codesize,design,unusedcode --exclude 'vendor','tests',,'c3.php' --ignore-violations-on-exit

build: ## Build docker image
	$(DOCKER_C) build

health: ## Check health of the docker image
	$(DOCKER_C) exec -e "WAIT_HOSTS=localhost:80" $(APP_NAME) /wait

logs: ## Watch docker log files
	$(DOCKER_C) logs -f

composer.install: ## Run composer install inside container
	$(DOCK_X_APP) composer install

composer.update: ## Run composer update inside container
	$(DOCK_X_APP) composer update

composer.normalize: src/vendor ## Run composer normalize inside container
	$(DOCK_X_APP) composer normalize

src/vendor:
	$(DOCK_X_APP) composer install

help:
	@grep -E '^[a-zA-Z._-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
